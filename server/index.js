'use strict';

// Register the Babel require hook
require('babel-core/register');

// Export the application
module.exports = require('./app');
